$(document).ready(function() {
  productTabInit();
  popupInit();
  animationInit();
  faqAccordionInit();
  videoPlayInit();
  sliderInit();
})

const productTabInit = function() {
  $(document).on('click', '.product-tab-info .product-tab-header', function() {
    var handle = $(this).attr('data-target');
    $('.product-tab-info .product-tab-header').removeClass('active');
    $(this).addClass('active');

    $('.product-tab-info .product-tab-content').removeClass('active');
    $('.product-tab-info .product-tab-content[data-handle="' + handle + '"]').addClass('active');

  })
};

const popupInit = function() {
  $('[data-popup-close]').click(function(){
    $(this).closest('[data-popup]').addClass('is-hidden');
    $("body").removeClass('popup-open');
    setTimeout(function(){
      $(this).closest('[data-popup]').css('opacity', '0');
    }, 1);
  })

  $('[data-popup-open]').click(function(event) {
    event.preventDefault();
    var popup = $(this).data('popup-open');
    var $popup = $('[data-popup="'+ popup +'"');
    $popup.toggleClass('is-hidden');
    $("body").toggleClass('popup-open');
    setTimeout(function(){
      $popup.css('opacity', '1');
    }, 1);
  });

  // Open popup when popup form loaded with errors
  $('[data-popup-form-errors]').each(function (index, element) {
    const popup = $(this).data('popup-form-errors');
    $(`[data-popup-open="${popup}"]`).trigger('click');
  });
}

const animationInit = function () {
  const sections = $('.section, [data-animate-section]');
  const blocks = $('[data-animate-block]');
  $(document).scroll(function () {
    const scroll = $(this).scrollTop();
    const windowHeight = $(window).outerHeight();
    const headerHeight = $("#announcement-bar-with-slider").outerHeight();
    sections.each(function (index, section) {
      const sectionTop = $(section).position().top - $(window).outerHeight()/1.5;
      if (scroll >= sectionTop) {
        $(section).addClass('section-in');
      }
    });
    blocks.each(function (index, block) {
      const blockTop = $(block).offset().top;
      const blockHeight = $(block).outerHeight();

      if ( $(block).find('[data-scroll-line]').length ) {
        let lineHeight = 0;
        let $lastActiveBlock = $(block).find('[data-animate-block].is-active').last();
        if ($lastActiveBlock.length) {
          const lastActiveBlockHeight = $lastActiveBlock.outerHeight();
          lineHeight = 100*(scroll + headerHeight + windowHeight / 2 - blockTop - lastActiveBlockHeight) / blockHeight;
        } else {
          $lastActiveBlock = $(block).find('[data-animate-block]').eq(0)
        }
        const imgUrl = $lastActiveBlock.data('image');
        $('[data-scroll-image]').attr('src', imgUrl);
        
        if (lineHeight >= 100) {
          $(block).addClass('is-active');
          $('[data-scroll-line]', $(block)).css('height', '100%');
        } else {
          $(block).removeClass('is-active');
          $('[data-scroll-line]', $(block)).css('height', lineHeight+'%');
        }

      } else {
        if (scroll + headerHeight + windowHeight / 2 >= blockTop + blockHeight) {
          $(block).addClass('is-active');
        } else {
          $(block).removeClass('is-active');
        }
      }
    });
  }).scroll();
  $(document).ready(function () {
    $('body').addClass('body-in');
  });
}


const faqAccordionInit = function() {
  $(document).on('click', '.faq-accordion-item .faq-accordion-header', function() {
    var $wrapper = $(this).closest('.faq_accordion-wrapper');
    var $item = $(this).closest('.faq-accordion-item');
    if(! $item.hasClass('opened')) {
      $wrapper.find('.faq-accordion-item.opened .faq-accordion-content').slideUp();
      $wrapper.find('.faq-accordion-item').removeClass('opened');
    }

    $item.toggleClass('opened');
    $item.find('.faq-accordion-content').slideToggle();
  })
}

const videoPlayInit = function() {

  $(document).scroll(function() {
    
    $('[video-autoplay]').each(function() {
      var $video = $(this);
      var _top = $video.offset().top;
      var s_top = $(document).scrollTop();

      if(s_top > _top + 150) {
        $video.get(0).play();
      }

    })

  })
}


const sliderInit = function() {
  $('.testimonial-slider').slick({
    centerMode: true,
    dots: true,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 749,
        settings: {
          centerMode: true,
          slidesToShow: 1,
          variableWidth: false
        }
      }
    ]
  });
}